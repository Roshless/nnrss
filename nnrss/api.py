"""API endpoints"""
import json

from flask import Blueprint, Response, jsonify, request, url_for
from werkzeug.exceptions import abort

from nnrss.database import RssHandler
from nnrss.decorators import api_key_required

API = Blueprint("api", __name__)


@API.route("/feed", methods=["GET"])
@api_key_required
def api_feeds_index():
    """Return all feeds of given api key user"""
    rss_handler = RssHandler()
    feed_list = rss_handler.get_feeds(api_feeds_index.api_key_username)
    output = [
        {
            "id": url_for("api.api_view_feed_entries", feed_id=f["id"], _external=True),
            "title": f["title"],
            "link": f["link"],
            "category": f["category"],
        }
        for f in feed_list
    ]
    return Response(json.dumps(output), mimetype="application/json")


@API.route("/feed", methods=["POST"])
@api_key_required
def api_feed_add():
    rss_handler = RssHandler()
    url = request.args.get("url")
    category = request.args.get("category")
    if url is None or category is None:
        return "Provide url and category parameters", 400
    try:
        feed_added = rss_handler.add_feeds(
            [url], api_feed_add.api_key_username, [category]
        )
    except AttributeError:
        return "Not a valid feed URL", 400
    if feed_added:
        return "Feed {} added!".format(url)
    return "Not a valid feed URL", 400


@API.route("/feed/<int:feed_id>", methods=["GET"])
@api_key_required
def api_view_feed_entries(feed_id=None):
    if feed_id is None:
        return "Enter feed_id after /feed/", 400
    rss_handler = RssHandler()
    entries = rss_handler.get_entries_by_feed_id(
        feed_id, api_view_feed_entries.api_key_username
    )
    if entries.count() == 0:
        abort(404)
    output = [
        {
            "id": entry.id,
            "title": entry.title,
            "link": entry.link,
            "date": entry.date.timestamp(),
            "is_read": entry.is_read,
        }
        for entry in entries.all()
    ]
    return Response(json.dumps(output), mimetype="application/json")


@API.route("/feed/<int:feed_id>", methods=["POST"])
@api_key_required
def api_set_read_entries(feed_id=None):
    entry_id = request.args.get("entry_id")
    if feed_id is None:
        return "Enter feed_id after /feed/ in URL", 400
    if feed_id is None or entry_id is None:
        return "entry_id parameter not present", 400
    rss_handler = RssHandler()
    is_ok, _ = rss_handler.set_read_entry(
        feed_id, entry_id, api_set_read_entries.api_key_username
    )
    if not is_ok:
        abort(404)
    return "Entry is_read is set to True now"


@API.route("/feed/<int:feed_id>", methods=["PATCH"])
@api_key_required
def api_update_feed(feed_id=None):
    if feed_id is None:
        return "Enter feed ID"
    rss_handler = RssHandler()
    if rss_handler.update_feed(feed_id, api_update_feed.api_key_username):
        return "Feed updated!"
    abort(404)


@API.route("/feed/<int:feed_id>", methods=["DELETE"])
@api_key_required
def api_remove_feed(feed_id=None):
    if feed_id is None:
        return "Enter feed ID"
    rss_handler = RssHandler()
    if rss_handler.remove_feed(feed_id, api_remove_feed.api_key_username):
        return "Feed removed!"
    abort(404)


@API.errorhandler(404)
def resource_not_found(exception):
    """Return error in json"""
    return jsonify(error=str(exception)), 404
