from functools import wraps

from flask import render_template, request, session, url_for
from werkzeug.utils import redirect

from nnrss.config import ACCOUNT
from nnrss.database import RssHandler


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if "logged_in" not in session:
            return redirect(url_for("default_web.login"))
        return f(*args, **kwargs)

    return decorated_function


def admin_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        rss_handler = RssHandler()
        if not rss_handler.is_user_admin(session["logged_in"]):
            return render_template("errors/permission_denied.html"), 403
        return f(*args, **kwargs)

    return decorated_function


def api_key_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        rss_handler = RssHandler()
        if not ACCOUNT.api_enabled:
            return "API disabled", 404

        api_key = request.headers.get("API-KEY")
        username = request.headers.get("USERNAME")
        if not api_key:
            return "No API-KEY header", 401
        if not username:
            return "No USERNAME header", 401

        api_username = rss_handler.is_api_valid(username, api_key)
        if not api_username:
            return "Wrong API key and/or username", 401

        decorated_function.api_key_username = api_username
        return f(*args, **kwargs)

    return decorated_function


def registration_enabled(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not ACCOUNT.is_registration_enabled():
            return render_template("errors/registration_disabled.html"), 403
        return f(*args, **kwargs)

    return decorated_function


def guest_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if "logged_in" in session:
            return redirect(url_for("default_web.index"))
        return f(*args, **kwargs)

    return decorated_function
