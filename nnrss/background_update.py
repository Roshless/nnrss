import atexit
from logging import basicConfig, info

from apscheduler.schedulers.background import BackgroundScheduler

from nnrss.database import RssHandler
from nnrss.models import User


class BackgroundUpdate(object):
    def __init__(self):
        self.scheduler = BackgroundScheduler()
        """scheduler.add_job(func=self.print_date_time,
                          trigger="interval",
                          minutes=1)"""
        basicConfig(format="%(asctime)s %(message)s", datefmt="%d/%m/%Y %I:%M:%S %p")
        # level=INFO debug
        self.start_timer()
        self.scheduler.start()
        # atexit.register(lambda: scheduler.shutdown())
        atexit.register(self.scheduler.shutdown)

    def start_timer(self):
        self.scheduler.remove_all_jobs()
        handler = RssHandler()
        users = handler.get_users()
        for user in users:
            # remove this lambda?
            self.scheduler.add_job(
                func=lambda u=user: self.__update_feeds(u),
                trigger="interval",
                id=user.username,
                minutes=user.update_interval,
            )
            info(
                "job started for user {}, interval {}".format(
                    user.username, user.update_interval
                )
            )

    def stop(self):
        self.scheduler.shutdown()

    def reschedule(self, username: str):
        handler = RssHandler()
        user = handler.get_user(username)
        self.scheduler.reschedule_job(
            username, trigger="interval", minutes=user.update_interval
        )
        info(
            "RESCHEDULE: username {}, new time {}m".format(
                username, user.update_interval
            )
        )
        # self.scheduler.remove_job(username)

    def add_new_user_job(self, username: str):
        handler = RssHandler()
        user = handler.get_user(username)
        self.scheduler.add_job(
            func=lambda u=user: self.__update_feeds(u),
            trigger="interval",
            id=user.username,
            minutes=user.update_interval,
        )
        info(
            "ADDED TO SCHEDULE: username {}, time {}m".format(
                user.username, user.update_interval
            )
        )

    # private

    def __update_feeds(self, user: User):
        rss_handler = RssHandler()
        feeds = rss_handler.get_feeds(user.username)
        for the_feed in feeds:
            rss_handler.update_feed(the_feed["id"])

    def __remove_old_feed_entries(self, user: User):
        pass


BACKGROUND_TASK = BackgroundUpdate()
